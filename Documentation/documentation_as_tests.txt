Documentation as tests
Let's discuss our sample example documentation:



/// ```
/// println!("Hello, world");
/// ```
You'll notice that you don't need a fn main() or anything here. rustdoc will automatically add a main() wrapper around your code, using heuristics to attempt to put it in the right place. For example:



/// ```
/// use std::rc::Rc;
///
/// let five = Rc::new(5);
/// ```
This will end up testing:


fn main() {
    use std::rc::Rc;
    let five = Rc::new(5);
}
Here's the full algorithm rustdoc uses to preprocess examples:

Any leading #![foo] attributes are left intact as crate attributes.
Some common allow attributes are inserted, including unused_variables, unused_assignments, unused_mut, unused_attributes, and dead_code. Small examples often trigger these lints.
If the example does not contain extern crate, then extern crate <mycrate>; is inserted (note the lack of #[macro_use]).
Finally, if the example does not contain fn main, the remainder of the text is wrapped in fn main() { your_code }.
This generated fn main can be a problem! If you have extern crate or a mod statements in the example code that are referred to by use statements, they will fail to resolve unless you include at least fn main() {} to inhibit step 4. #[macro_use] extern crate also does not work except at the crate root, so when testing macros an explicit main is always required. It doesn't have to clutter up your docs, though -- keep reading!

Sometimes this algorithm isn't enough, though. For example, all of these code samples with /// we've been talking about? The raw text:


/// Some documentation.
# fn foo() {}
looks different than the output:



/// Some documentation.
Yes, that's right: you can add lines that start with #, and they will be hidden from the output, but will be used when compiling your code. You can use this to your advantage. In this case, documentation comments need to apply to some kind of function, so if I want to show you just a documentation comment, I need to add a little function definition below it. At the same time, it's only there to satisfy the compiler, so hiding it makes the example more clear. You can use this technique to explain longer examples in detail, while still preserving the testability of your documentation.

For example, imagine that we wanted to document this code:



let x = 5;
let y = 6;
println!("{}", x + y);
We might want the documentation to end up looking like this:

First, we set x to five:



let x = 5;
Next, we set y to six:



let y = 6;
Finally, we print the sum of x and y:



println!("{}", x + y);
To keep each code block testable, we want the whole program in each block, but we don't want the reader to see every line every time. Here's what we put in our source code:


    First, we set `x` to five:

    ```rust
    let x = 5;
    # let y = 6;
    # println!("{}", x + y);
    ```

    Next, we set `y` to six:

    ```rust
    # let x = 5;
    let y = 6;
    # println!("{}", x + y);
    ```

    Finally, we print the sum of `x` and `y`:

    ```rust
    # let x = 5;
    # let y = 6;
    println!("{}", x + y);
    ```
By repeating all parts of the example, you can ensure that your example still compiles, while only showing the parts that are relevant to that part of your explanation.

Documenting macros
Here’s an example of documenting a macro:


/// Panic with a given message unless an expression evaluates to true.
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate foo;
/// # fn main() {
/// panic_unless!(1 + 1 == 2, “Math is broken.”);
/// # }
/// ```
///
/// ```rust,should_panic
/// # #[macro_use] extern crate foo;
/// # fn main() {
/// panic_unless!(true == false, “I’m broken.”);
/// # }
/// ```
#[macro_export]
macro_rules! panic_unless {
    ($condition:expr, $($rest:expr),+) => ({ if ! $condition { panic!($($rest),+); } });
}
You’ll note three things: we need to add our own extern crate line, so that we can add the #[macro_use] attribute. Second, we’ll need to add our own main() as well (for reasons discussed above). Finally, a judicious use of # to comment out those two things, so they don’t show up in the output.

Another case where the use of # is handy is when you want to ignore error handling. Lets say you want the following,


/// use std::io;
/// let mut input = String::new();
/// try!(io::stdin().read_line(&mut input));
The problem is that try! returns a Result<T, E> and test functions don't return anything so this will give a mismatched types error.


/// A doc test using try!
///
/// ```
/// use std::io;
/// # fn foo() -> io::Result<()> {
/// let mut input = String::new();
/// try!(io::stdin().read_line(&mut input));
/// # Ok(())
/// # }
/// ```
You can get around this by wrapping the code in a function. This catches and swallows the Result<T, E> when running tests on the docs. This pattern appears regularly in the standard library.

Running documentation tests
To run the tests, either:


$ rustdoc --test path/to/my/crate/root.rs
# or
$ cargo test
That's right, cargo test tests embedded documentation too. However, cargo test will not test binary crates, only library ones. This is due to the way rustdoc works: it links against the library to be tested, but with a binary, there’s nothing to link to.

There are a few more annotations that are useful to help rustdoc do the right thing when testing your code:



/// ```rust,ignore
/// fn foo() {
/// ```
The ignore directive tells Rust to ignore your code. This is almost never what you want, as it's the most generic. Instead, consider annotating it with text if it's not code, or using #s to get a working example that only shows the part you care about.



/// ```rust,should_panic
/// assert!(false);
/// ```
should_panic tells rustdoc that the code should compile correctly, but not actually pass as a test.



/// ```rust,no_run
/// loop {
///     println!("Hello, world");
/// }
/// ```
The no_run attribute will compile your code, but not run it. This is important for examples such as "Here's how to retrieve a web page," which you would want to ensure compiles, but might be run in a test environment that has no network access.

Documenting modules
Rust has another kind of doc comment, //!. This comment doesn't document the next item, but the enclosing item. In other words:



mod foo {
    //! This is documentation for the `foo` module.
    //!
    //! # Examples

    // ...
}
This is where you'll see //! used most often: for module documentation. If you have a module in foo.rs, you'll often open its code and see this:



//! A module for using `foo`s.
//!
//! The `foo` module contains a lot of useful functionality blah blah blah...
Crate documentation
Crates can be documented by placing an inner doc comment (//!) at the beginning of the crate root, aka lib.rs:



//! This is documentation for the `foo` crate.
//!
//! The foo crate is meant to be used for bar.
Documentation comment style
Check out RFC 505 for full conventions around the style and format of documentation.

Other documentation
All of this behavior works in non-Rust source files too. Because comments are written in Markdown, they're often .md files.

When you write documentation in Markdown files, you don't need to prefix the documentation with comments. For example:



/// # Examples
///
/// ```
/// use std::rc::Rc;
///
/// let five = Rc::new(5);
/// ```
is:


# Examples

```
use std::rc::Rc;

let five = Rc::new(5);
```
when it's in a Markdown file. There is one wrinkle though: Markdown files need to have a title like this:


% The title

This is the example documentation.
This % line needs to be the very first line of the file.

doc attributes
At a deeper level, documentation comments are syntactic sugar for documentation attributes:



/// this

#[doc="this"]
are the same, as are these:



//! this

#![doc="this"]
You won't often see this attribute used for writing documentation, but it can be useful when changing some options, or when writing a macro.

Re-exports
rustdoc will show the documentation for a public re-export in both places:


extern crate foo;

pub use foo::bar;
This will create documentation for bar both inside the documentation for the crate foo, as well as the documentation for your crate. It will use the same documentation in both places.

This behavior can be suppressed with no_inline:


extern crate foo;

#[doc(no_inline)]
pub use foo::bar;