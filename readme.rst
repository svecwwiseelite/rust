**Introduction:**

Rust is a multi-paradigm programming language focused on performance and safety, especially safe concurrency. Rust is syntactically similar to C++, also providing memory safety without using garbage collection. The concrete syntax of Rust is similar to C and C++, with blocks of code delimited by curly brackets, and control flow keywords such as if, else, while, and for. 

**Features:**

* Pattern Matching

**Pre-requisites:**

* Ensure that sudo package and curl are installed.

**Installation:**

* sudo apt-get update
* sudo apt-get upgrade
* curl https://sh.rustup.rs -sSf | sh



**To check the version:**

* rust --version

**Resources**

* `Rust Documentation <https://www.rust-lang.org/learn>`_

* `Rust The Basics <https://stepik.org/lesson/9268/step/1>`_

* `24 Days of Rust Article <https://zsiciarz.github.io/24daysofrust/>`_

* `The Embedded Rust Book <https://rust-embedded.github.io/book/intro/index.html?ref=hackr.io>`_

* `Rust Tutorial <https://aml3.github.io/RustTutorial/html/toc.html?ref=hackr.io>`_

* `Rust for System Programmers <https://github.com/nrc/r4cppp?ref=hackr.io>`_

* `Learning Rust with Entirely too Many Linked Lists <http://cglab.ca/~abeinges/blah/too-many-lists/book/README.html?ref=hackr.io>`_

* `The Rustonomicon <https://doc.rust-lang.org/stable/nomicon/>`_

* `A Gentle Introduction to Rust <https://stevedonovan.github.io/rust-gentle-intro/>`_

* `Why Rust <https://www.oreilly.com/programming/free/files/why-rust.pdf>`_

