fn main(){
        fizzbuzz(50);
}


fn fizzbuzz(mut num:i32) {
	let mut v:Vec<String> = Vec::new();
	for x in 1..num+1{
		if (x % 5 == 0 &&  x % 3 == 0) {
			v.push("FizzBuzz".to_string());
		}
		else if (x % 5 == 0){
			v.push("Buzz".to_string());
		}
		else if (x % 3 == 0){
			v.push("Fizz".to_string());
		}
		else{
			v.push(x.to_string());
		}
	}
	println!("{:?}",v);
}
 
